import socket

def query(vals):
    return '&'.join(str(k)+'='+str(v) for k,v in vals.iteritems())

info = {'id':'testservice','grade':'service','group':'Testservices','desc':'Test','type':'string', 'access':'rw'}

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
s.settimeout(5)

msg = query(info)
s.sendto(msg, ("<broadcast>", 31338))
