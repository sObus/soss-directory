#!/usr/bin/env python2
import json
from flask import Flask, Response
from SocketServer import UDPServer, BaseRequestHandler
from threading import Thread
from time import time, sleep

SERVICE_TIMEOUT = 12
CHECK_INTERVAL = 2

services = []
events = []
running = False

app = Flask(__name__)
@app.route("/services")
def servicesJSON():
    return Response(json.dumps(services), mimetype="application/json")

@app.route("/events")
def eventsJSON():
    return Response(json.dumps(events), mimetype="application/json")

def parseAnnounce(msg):
    return dict([tuple(s.split('=',1)) for s in msg.split('&')])

class Handler(BaseRequestHandler):
    def handle(self):
        print "message:", self.request[0]
        anc = parseAnnounce(self.request[0])
        if 'grade' in anc and 'id' in anc and 'group' in anc and 'desc' in anc and 'type' in anc and 'access' in anc:
            if anc['grade'] == 'service':
                target = services
            elif anc['grade'] == 'event':
                target = events
            else:
                return
            anc['lastseen'] = int(time())
            for obj in target:
                if obj['id'] == anc['id']:
                    keys = list(obj.iterkeys())
                    for k in keys:
                        del obj[k]
                    for k in anc:
                        obj[k] = anc[k]
                    break
            else:
                target.append(anc)

class CustomUDPServer(UDPServer):
    def __init__(self, addr, handler):
        self.allow_reuse_address = True
        UDPServer.__init__(self, addr, handler)

server = CustomUDPServer(('',31338), Handler)
def udplisten():
    server.serve_forever()

def timeoutcheck(v, now):
    return v['lastseen']+SERVICE_TIMEOUT >= now

def timer():
    while running:
        try:
            sleep(CHECK_INTERVAL)
        except KeyboardInterrupt:
            break
        now = int(time())
        services[:] = [v for v in services if timeoutcheck(v, now)]
        events[:] = [v for v in events if timeoutcheck(v, now)]

if __name__ == "__main__":
    running = True
    t = Thread(target=udplisten, name="UDP Server")
    t.start()
    t2 = Thread(target=timer, name="Timer")
    t2.start()
    app.run(debug=True, use_reloader=False)
    running = False
    server.shutdown()
    t2.join()

